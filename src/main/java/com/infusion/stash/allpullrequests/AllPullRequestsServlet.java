package com.infusion.stash.allpullrequests;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.project.ProjectService;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.pull.PullRequestState;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.infusion.stash.allpullrequests.service.AllPullRequestService;
import com.infusion.stash.allpullrequests.service.PlatformService;

public class AllPullRequestsServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = -8400576920477105409L;
    
    private final ProjectService projectService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final PageBuilderService pageBuilderService;
    private final AuthenticationContext authenticationContext;
    private final AllPullRequestService allPullRequestService;
    private final PlatformService platformService;

    public AllPullRequestsServlet(final ProjectService projectService,
                                  final PullRequestService pullRequestService,
                                  final SoyTemplateRenderer soyTemplateRenderer,
                                  final PageBuilderService pageBuilderService,
                                  final AuthenticationContext authenticationContext,
                                  final PullRequestExtendedFactory pullRequestExtendedFactory) {
        this.projectService = projectService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.pageBuilderService = pageBuilderService;
        this.authenticationContext = authenticationContext;
        this.allPullRequestService = new AllPullRequestService(pullRequestService, pullRequestExtendedFactory);
        this.platformService = new PlatformService(projectService);
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    	Project project = null;
    	Set<Integer> projectIds = Sets.newHashSet();
        final String[] path = request.getPathInfo().split("/");
        if (path.length == 2 && "all".equals(path[1])) {
            project = null;
        } else if (path.length == 3 && "project".equals(path[1]) && !path[2].isEmpty()) {
            String projectKey = path[2];
            project = projectService.getByKey(projectKey);
            projectIds.add(project.getId());
        }
        else if (path.length == 3 && "platform".equals(path[1]) && !path[2].isEmpty()) {
            String platformKey = path[2];
            projectIds.addAll(platformService.getProjectIds(platformKey));
        }
        else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        String activeTab = request.getParameter("state");
        if (activeTab == null) {
        	activeTab = "open";
        }
        PullRequestState state = AllPullRequestService.getStateFromStr(activeTab);

        final PageRequest pageRequest = new PageRequestImpl(0, AllPullRequestService.MAX_RESULTS);
        final Page<PullRequestExtended> pullRequestPage = projectIds.size() == 0 ? this.allPullRequestService.findPullRequests(project, state, pageRequest) :
        	this.allPullRequestService.findPullRequests(projectIds, state, pageRequest);

        final Map<String, Object> context = Maps.newHashMap();
        context.put("pullRequestPage", pullRequestPage);
        context.put("activeTab", activeTab);
        context.put("currentUser", authenticationContext.getCurrentUser());

        String template;
        if (project == null) {
            pageBuilderService.assembler().resources().requireContext("com.infusion.stash.stash-all-pull-requests.all");
            template = "plugin.page.allPullRequests";
        }
        else {
            pageBuilderService.assembler().resources().requireContext("com.infusion.stash.stash-all-pull-requests.project");
            context.put("project", project);
            template = "plugin.page.projectPullRequests";
        }

        response.setContentType("text/html; charset=UTF-8");
        try {
            soyTemplateRenderer.render(
                    response.getWriter(),
                    "com.infusion.stash.stash-all-pull-requests:server-side-soy",
                    template, context);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                throw new ServletException(e);
            }
        }
    }
}
