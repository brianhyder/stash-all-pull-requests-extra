package com.infusion.stash.allpullrequests.rest;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.project.ProjectService;
import com.atlassian.bitbucket.pull.PullRequestDirection;
import com.atlassian.bitbucket.pull.PullRequestSearchRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.pull.PullRequestState;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.rest.RestResource;
import com.atlassian.bitbucket.rest.util.ResponseFactory;
import com.atlassian.bitbucket.rest.util.RestUtils;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.Maps;
import com.infusion.stash.allpullrequests.ExtendedPullRequest;
import com.infusion.stash.allpullrequests.PullRequestExtended;
import com.infusion.stash.allpullrequests.PullRequestExtendedFactory;
import com.infusion.stash.allpullrequests.service.AllPullRequestService;
import com.sun.jersey.spi.resource.Singleton;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Singleton
@AnonymousAllowed
public class AllPullRequestsResource extends RestResource {

    private final PullRequestService pullRequestService;
    private final RepositoryService repositoryService;
    private final PermissionValidationService permissionValidationService;
	private final AllPullRequestService allPullRequestService;
	private final ProjectService projectService;

    public AllPullRequestsResource(PullRequestService pullRequestService, 
    		RepositoryService repositoryService, 
    		PermissionValidationService permissionValidationService, 
    		I18nService i18nService,
    		PullRequestExtendedFactory pullRequestExtendedFactory,
    		ProjectService projectService) {
        super(i18nService);
        this.pullRequestService = pullRequestService;
        this.repositoryService = repositoryService;
        this.permissionValidationService = permissionValidationService;
        this.allPullRequestService = new AllPullRequestService(pullRequestService, pullRequestExtendedFactory);
        this.projectService = projectService;
     }

    @GET
    @Path("count")
    public Response getPullRequestCount(@QueryParam("project") String projectKey) {
        permissionValidationService.validateAuthenticated();

        PageRequest pageRequest = new PageRequestImpl(0, 10);
        long count = 0;
        while (pageRequest != null) {
            Page<? extends Repository> page = repositoryService.findByProjectKey(projectKey, pageRequest);
            for (Repository repository : page.getValues()) {
                count += pullRequestService.count(
                        new PullRequestSearchRequest.Builder()
                                .state(PullRequestState.OPEN)
                                .repositoryAndBranch(PullRequestDirection.INCOMING, repository.getId(), null)
                                .build());
            }
            if (page.getIsLastPage()) {
                break;
            }
            pageRequest = page.getNextPageRequest();
        }

        Map<String, Long> response = Maps.newHashMap();
        response.put("count", count);

        return ResponseFactory.ok(response).build();
    }

    @GET
    @Path("all")
    public Response getAllPullRequests(@QueryParam("project") String projectKey, @QueryParam("state") String stateStr) {
    	
    	//fetch project if defined
    	Project project = null;
    	if (projectKey != null) {
    		project = projectService.getByKey(projectKey);
    	}
    	
    	//fetch state
        if (stateStr == null) {
        	stateStr = "open";
        }
        PullRequestState state = AllPullRequestService.getStateFromStr(stateStr);

        final PageRequest pageRequest = new PageRequestImpl(0, AllPullRequestService.MAX_RESULTS);
        final Page<PullRequestExtended> pullRequestPage = this.allPullRequestService.findPullRequests(project, state, pageRequest);
        
        ArrayList<ExtendedPullRequest> prs = new ArrayList<ExtendedPullRequest>(pullRequestPage.getSize());
        for (PullRequestExtended extendedPullRequest : pullRequestPage.getValues()) {
			prs.add(ExtendedPullRequest.create(extendedPullRequest));
		}
        
        Map<String, Object> response = Maps.newHashMap();
        response.put("count", pullRequestPage.getSize());
        response.put("data", prs);
        return ResponseFactory.ok(response).build();
    }
}
