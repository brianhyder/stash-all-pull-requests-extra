package com.infusion.stash.allpullrequests;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class Project {

	public int id;
	public String key;
	public String description;
	public String name;

	public static Project create(com.atlassian.bitbucket.project.Project pr) {
		
		Project p = new Project();
		p.id = pr.getId();
		p.key = pr.getKey();
		p.description = pr.getDescription();
		p.name = pr.getName();
		
		return p;
	}
}
