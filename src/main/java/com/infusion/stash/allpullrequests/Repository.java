package com.infusion.stash.allpullrequests;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class Repository {

	public String statusMessage;
	public String slug;
	public String name;
	public String hierarchyId;
	public int id;
	public Project project;

	public static Repository create(com.atlassian.bitbucket.repository.Repository rp) {
		
		Repository r = new Repository();
		r.id = rp.getId();
		r.hierarchyId = rp.getHierarchyId();
		r.name = rp.getName();
		r.slug = rp.getSlug();
		r.statusMessage = rp.getStatusMessage();
		r.project = Project.create(rp.getProject());
		
		return r;
	}
}
