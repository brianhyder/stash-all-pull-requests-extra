package com.infusion.stash.allpullrequests;

public class PullRequestRef {
	
	public String id;
	public String displayId;
	public String latestCommit;
	public Repository repository;

	public static PullRequestRef create(com.atlassian.bitbucket.pull.PullRequestRef rf) {
		
		PullRequestRef p = new PullRequestRef();
		p.id = rf.getId();
		p.displayId = rf.getDisplayId();
		p.latestCommit = rf.getLatestCommit();
		p.repository = Repository.create(rf.getRepository());
		
		return p;
	}
}
