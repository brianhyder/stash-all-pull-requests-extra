package com.infusion.stash.allpullrequests.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.Map.Entry;

import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestOrder;
import com.atlassian.bitbucket.pull.PullRequestSearchRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.pull.PullRequestState;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageImpl;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.infusion.stash.allpullrequests.PullRequestExtended;
import com.infusion.stash.allpullrequests.PullRequestExtendedFactory;

public class AllPullRequestService {
	
	public static final int MAX_RESULTS_PER_PAGE = 10;
	public static final int MAX_RESULTS = 100;
	
	protected static final HashMap<String, PullRequestState> PR_STATE_STR_TO_STATE = Maps.newHashMap();
	static {
		PR_STATE_STR_TO_STATE.put("open", PullRequestState.OPEN);
		PR_STATE_STR_TO_STATE.put("merged", PullRequestState.MERGED);
		PR_STATE_STR_TO_STATE.put("declined", PullRequestState.DECLINED);
	}

	private PullRequestService pullRequestService;
	private PullRequestExtendedFactory pullRequestExtendedFactory;
	
	public AllPullRequestService(final PullRequestService pullRequestService,
			final PullRequestExtendedFactory pullRequestExtendedFactory) {
		this.pullRequestService = pullRequestService;
		this.pullRequestExtendedFactory = pullRequestExtendedFactory;
	}
	
	public Page<PullRequestExtended> findPullRequests(final Project project, final PullRequestState state, final PageRequest pageRequest) {
        final PullRequestSearchRequest searchRequest = (new PullRequestSearchRequest.Builder()).
                state(state).order(PullRequestOrder.NEWEST).build();

        //we want all the things
        if (project == null) {
            return this.findAllPullRequests(searchRequest, pageRequest);
        }

        // unfortunately, we can't use any PullRequestSearchRequest filter for this :/
        Set<Integer> projectIds = new HashSet<Integer>();
        projectIds.add(project.getId());
        return this.findPullRequests(projectIds, searchRequest, pageRequest);
    }
	
	public Page<PullRequestExtended> findAllPullRequests(final PullRequestSearchRequest searchRequest, PageRequest pageRequest) {
		final Page<PullRequest> page = pullRequestService.search(searchRequest, pageRequest);
        final SortedMap<Integer, PullRequest> pageRequestsMap = page.getOrdinalIndexedValues();
        final List<PullRequestExtended> values = Lists.newLinkedList();
        for(Entry<Integer, PullRequest> entry: pageRequestsMap.entrySet()) {
            final PullRequest pullRequest = entry.getValue();
            final PullRequestExtended pullRequestExtended = pullRequestExtendedFactory.getPullRequestExtended(pullRequest);
            values.add(pullRequestExtended);
        }
        return new PageImpl<PullRequestExtended>(pageRequest, values, page.getIsLastPage());
	}
	
	public Page<PullRequestExtended> findPullRequests(final Set<Integer> projectIds, final PullRequestState state, final PageRequest pageRequest) {
		final PullRequestSearchRequest searchRequest = (new PullRequestSearchRequest.Builder()).
                state(state).order(PullRequestOrder.NEWEST).build();
		
		return this.findPullRequests(projectIds, searchRequest, pageRequest);
	}
	
	public Page<PullRequestExtended> findPullRequests(final Set<Integer> projectIds, final PullRequestSearchRequest searchRequest, final PageRequest pageRequest) {
		final List<PullRequestExtended> values = Lists.newLinkedList();
        boolean isLastPage = false;

        int offset = 0;
        PageRequest tmpPageRequest = new PageRequestImpl(0, MAX_RESULTS_PER_PAGE);
        while (tmpPageRequest != null && values.size() < pageRequest.getLimit() && !isLastPage) {
            final Page<PullRequest> pullRequestPage = pullRequestService.search(searchRequest, tmpPageRequest);
            if (pullRequestPage.getIsLastPage()) {
                isLastPage = true;
            }
            for (PullRequest pullRequest : pullRequestPage.getValues()) {
            	
            	int prProjectId = pullRequest.getToRef().getRepository().getProject().getId();
                if (projectIds.contains(prProjectId)) {
                    if (offset >= pageRequest.getStart() && values.size() < pageRequest.getLimit()) {
                        final PullRequestExtended pullRequestExtended = pullRequestExtendedFactory.getPullRequestExtended(pullRequest);
                        values.add(pullRequestExtended);
                    }
                    offset += 1;
                }
            }
            tmpPageRequest = pullRequestPage.getNextPageRequest();
        }

        return new PageImpl<PullRequestExtended>(pageRequest, values, isLastPage);
	}
	
	public static PullRequestState getStateFromStr(String activeTab){
		if (activeTab == null) {
			activeTab = "open";
		}
		
		PullRequestState state = PR_STATE_STR_TO_STATE.get(activeTab);
		if (state == null) {
			state = PullRequestState.OPEN;
		}
		return state;
	}
}
