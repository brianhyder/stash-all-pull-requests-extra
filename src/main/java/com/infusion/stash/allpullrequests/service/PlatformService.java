package com.infusion.stash.allpullrequests.service;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.project.ProjectService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class PlatformService {
	
	protected static final Map<String, Set<String>> PLATFORM_TO_PROJECTS = Maps.newHashMap();
	static {
		PLATFORM_TO_PROJECTS.put("dp", Sets.newHashSet("DEV_OPS", "DSI", "DSI_API", "DSI_DATA", "DSI_DOMAIN", "DSI_INTEGRATION", "DSI_MOBILE", "DSI_QLIK", "DSI_TEST", "DSI_TOOLS", "DSI_WEB", "DSI_WORKER","DSI_LIB_NETSTANDARD"));
		PLATFORM_TO_PROJECTS.put("m311", Sets.newHashSet("M311", "M311_CITIZENREQUEST", "M311_CONNECTGIS", "M311_MOBILE", "M311_SHARED", "M311WEB", "M311WORKER"));
		PLATFORM_TO_PROJECTS.put("twh", Sets.newHashSet("TWH", "TWHIN"));
		PLATFORM_TO_PROJECTS.put("classic", Sets.newHashSet("ACL", "CLAS"));
		PLATFORM_TO_PROJECTS.put("scalable", Sets.newHashSet("SCAL"));
		PLATFORM_TO_PROJECTS.put("eventpublisher", Sets.newHashSet("EVENT_PUBLISHER"));
		PLATFORM_TO_PROJECTS.put("ae", Sets.newHashSet("AE"));
	}
	
	private ProjectService projectService;

	public PlatformService (final ProjectService projectService) {
		this.projectService = projectService;
	}
	
	public Set<Integer> getProjectIds(String platformKey) {
		if (!PLATFORM_TO_PROJECTS.containsKey(platformKey)) {
			throw new RuntimeException("Invalid platform key: " + platformKey);
		}
		
		//no way that i've found to filter the search for projects so let's get them all to save DB calls
		Set<String> projectKeys = PLATFORM_TO_PROJECTS.get(platformKey);
		Set<Integer> projectIds = Sets.newHashSet();
		Page<Project> projects = projectService.findAll(new PageRequestImpl(0, 100));
		for(Iterator<Project> i = projects.getValues().iterator(); i.hasNext(); ) {
			Project p = i.next();
			if (projectKeys.contains(p.getKey())) {
				projectIds.add(p.getId());
			}
			
			if (projectIds.size() == projectKeys.size()) {
				//we've found all the keys that we are looking for
				break;
			}
		}
		
		return projectIds;
	}
}
