package com.infusion.stash.allpullrequests;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import com.atlassian.bitbucket.property.PropertyMap;
import com.infusion.stash.allpullrequests.PullRequestRef;

@JsonAutoDetect
public class ExtendedPullRequest {

	public Long id;
	public String title;
	public String description;
	public Date createdDate;
	public Date updatedDate;
	public long openedTasksCount;
	public PropertyMap properties;
	public int version;
	public List<String> vetos;
	public boolean isClosed;
	public boolean isCrossRepository;
	public boolean locked;
	public boolean isMergeable;
	public boolean isOpen;
	public PullRequestRef fromRef;
	public PullRequestRef toRef;

	public static ExtendedPullRequest create(PullRequestExtended pr) {
		
		ExtendedPullRequest p = new ExtendedPullRequest();
		p.id = pr.getId();
		p.title = pr.getTitle();
		p.description = pr.getDescription();
		p.createdDate = pr.getCreatedDate();
		p.updatedDate = pr.getUpdatedDate();
		p.openedTasksCount = pr.getOpenedTasksCount();
		p.properties = pr.getProperties();
		p.version = pr.getVersion();
		p.vetos = pr.getVetos();
		p.isClosed = pr.isClosed();
		p.isCrossRepository = pr.isCrossRepository();
		p.locked = pr.isLocked();
		p.isMergeable = pr.isMergeable();
		p.isOpen = pr.isOpen();
		p.fromRef = PullRequestRef.create(pr.getFromRef());
		p.toRef = PullRequestRef.create(pr.getToRef());
		
		return p;
	}
}
